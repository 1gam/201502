﻿using UnityEngine;
using System.Collections;

public class GuiCoins : MonoBehaviour {

	public tk2dTextMesh			textCoins;
	
	void Update () {
		textCoins.text = GameManager.Instance.GameCoins.ToString();
	}
}
