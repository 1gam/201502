﻿using UnityEngine;
using System.Collections;

public class GuiFuel : MonoBehaviour {

	public tk2dTextMesh			textFuel;
	
	void Update () {
		textFuel.text = GameManager.Instance.GameFuel.ToString("F");
	}
}
