﻿using UnityEngine;
using System.Collections;

public class GuiScore : MonoBehaviour {

	public tk2dTextMesh			textScore;
	
	void Update () {
		textScore.text = GameManager.Instance.GameScore.ToString();
	}
	
}
