﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;

public static class DataHelper {
	
	//	load some data from a file
	public static string LoadDataFromFile(string filename) {
		D.Trace("[GameDataHelpers] LoadDataFromFile");
		D.Log("- loading asset {0} from resources", filename);
		TextAsset ta = Resources.Load(filename) as TextAsset;
		D.Detail("- data loaded:\n{0}", ta.text);
		return ta.text;;
	}
	
	public static List<Dictionary<string, object>> LoadJsonDataFromFile(string filename, string key) {
		D.Trace("[GameDataHelpers] LoadJsonDataFromFile");
		string json = LoadDataFromFile(filename);
		Dictionary<string, List<Dictionary<string, object>>> list = JsonConvert.DeserializeObject<Dictionary<string, List<Dictionary<string, object>>>>(json);
		if (list.ContainsKey(key)) {
			return list[key];
		} else {
			return null;
		}
	}
	
}
