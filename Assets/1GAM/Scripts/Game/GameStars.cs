﻿using UnityEngine;
using System.Collections;

public class GameStars : MonoBehaviour {

	public GameObject			starPrefab;		//	
	public Vector2				distance;			//	distance to objective
	public int					spread;
	public int					stars;
	
	// Use this for initialization
	void Start () {
		
		for(int i = 0; i < Random.Range(stars/2,stars); i++) {
			
			float y = Random.Range(distance.x, distance.y);
			float x = Random.Range(-spread,spread);
			Vector3 pos = new Vector3(x,y,0.0f);
			
			GameObject go = (GameObject)Instantiate(starPrefab);
			go.transform.parent = transform;
			go.isStatic = true;
			go.transform.position = pos + new Vector3(0,0,0.1f);
			
		}	
	}
	
}
