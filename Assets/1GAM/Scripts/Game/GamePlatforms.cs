﻿using UnityEngine;
using System.Collections;

public class GamePlatforms : MonoBehaviour {

	public GameObject			platformPrefab;		//	
	public GameObject			fuelPrefab;			//
	public Vector2				distance;			//	distance to objective
	public int					spread;
	public int					platforms;

	// Use this for initialization
	void Start () {
		
		for(int i = 0; i < Random.Range(250,platforms); i++) {
		
			float y = Random.Range(distance.x, distance.y);
			float x = Random.Range(-spread,spread);
			Vector3 pos = new Vector3(x,y,0.0f);
			
			GameObject go = (GameObject)Instantiate(platformPrefab);
			go.transform.parent = transform;
			go.isStatic = true;
			go.transform.position = pos + new Vector3(0,0,0.1f);
			
			/*
			if (Random.Range(0,100) > 60) {
				GameObject fuel = (GameObject)Instantiate(fuelPrefab);
				fuel.transform.parent = transform;
				fuel.transform.position = pos;
			}
			*/
		}	
	}
	
}
