﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameSmoke : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.localScale = new Vector3(0.1f, 0.1f, 1);
		Destroy(gameObject, 2);
		HOTween.To(transform, 2.0f, "localScale", new Vector3(2.0f, 2.0f, 1));			
	}
	
}
