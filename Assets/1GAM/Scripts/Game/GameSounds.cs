﻿using UnityEngine;
using System.Collections;

public class GameSounds : MonoBehaviour {

	public AudioClip			GameCoin;
	public AudioClip			GamePickup;
	public AudioClip			GameWin;
	public AudioClip			GameLose;

	public void PlayWin() {
		playSound(GameWin);
	}
	
	public void PlayLose() {
		playSound(GameLose);
	}
	
	public void PlayPickup() {
		playSound(GamePickup);
	}
	
	public void PlayCoin() {
		playSound(GameCoin);
	}

	//	PRIVATE
	
	private void playPitchSound(AudioClip clip) {
		playSound(clip, Random.Range(0.2f,0.8f), 1.0f);
	}
	
	private void playSound(AudioClip clip) {
		
		D.Trace("[GameSounds] playSound");
		playSound(clip, 1.0f, 0.5f);
	}
	
	private void playSound(AudioClip clip, float pitch, float volume) {
		
		D.Trace("[GameSounds] playSound");
		
		GameObject go = new GameObject("Temp(Sound)");
		AudioSource gs = go.AddComponent<AudioSource>();
		gs.clip = clip;
		gs.volume = volume;
		gs.pitch = pitch;
		gs.Play();
		Destroy(go, clip.length + 0.1f);
	}
	
	private GameObject playSoundLoop(AudioClip clip) {
		
		D.Trace("[GameSounds] playSound");
		//audio.pitch = pitch;
		//audio.PlayOneShot(clip, volume);
		//	reset
		//audio.pitch = 1.0f;
		
		GameObject go = new GameObject("Sound (Temp)");
		AudioSource gs = go.AddComponent<AudioSource>();
		gs.clip = clip;
		gs.loop = true;
		gs.Play();
		return go;
	}
	
	void OnEnable() {
		GameManager.Instance.GameSounds = this;
	}
	
	
}
