﻿using UnityEngine;
using System.Collections;

public class GameFuel : MonoBehaviour {

	public int				FuelAmount;

	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Player") {
			GameManager.Instance.GameFuel += FuelAmount;
			GameManager.Instance.GameScore += 10;
			GameManager.Instance.GameSounds.PlayPickup();
			Destroy(gameObject);
		}
	}
}
