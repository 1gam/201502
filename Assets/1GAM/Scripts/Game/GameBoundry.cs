﻿using UnityEngine;
using System.Collections;

public class GameBoundry : MonoBehaviour {

	public GameObject			target;				//	whom are we watching
	public float				warningBoundry;		//	when to show warnings	
	public float				deathBoundry;		//	when you die
	public tk2dSlicedSprite		warningLeft;		//	left boundry flasher
	public tk2dSlicedSprite		warningRight;		//	right boundry flasher
	public tk2dTextMesh			warningText;		//	flashy message
	public tk2dSprite			borderRight;
	public tk2dSprite			borderLeft;
	
	public bool					Active;

	void OnEnable() {
		warningLeft.renderer.enabled = false;
		warningRight.renderer.enabled = false;
		Active = true;
	}
	
	void LateUpdate() {
	
		if (!Active) return;
	
		if (target.transform.position.x < -deathBoundry) {
			warningLeft.renderer.enabled = false;
			warningRight.renderer.enabled = false;
			warningText.renderer.enabled = false;
			borderRight.renderer.enabled = false;
			borderLeft.renderer.enabled = false;
			audio.volume = 0.0f;
			Active = false;
			GameManager.Instance.GameController.GameLoser();
			return;
		} 
		
		if (target.transform.position.x > deathBoundry) {
			warningLeft.renderer.enabled = false;
			warningRight.renderer.enabled = false;
			warningText.renderer.enabled = false;
			borderRight.renderer.enabled = false;
			borderLeft.renderer.enabled = false;
			audio.volume = 0.0f;
			Active = false;
			GameManager.Instance.GameController.GameLoser();
			return;
		} 
		
		if (target.transform.position.x < -warningBoundry) {
			warningLeft.renderer.enabled = true;
			warningText.renderer.enabled = true;
			borderLeft.renderer.enabled = true;
			audio.volume = 1.0f;
		} 
		
		if (target.transform.position.x > warningBoundry) {
			warningRight.renderer.enabled = true;
			warningText.renderer.enabled = true;
			borderRight.renderer.enabled = true;
			audio.volume = 1.0f;
		}
		
		if (target.transform.position.x > -warningBoundry && target.transform.position.x < warningBoundry) {
			warningLeft.renderer.enabled = false;
			warningRight.renderer.enabled = false;
			warningText.renderer.enabled = false;
			borderRight.renderer.enabled = false;
			borderLeft.renderer.enabled = false;
			audio.volume = 0.0f;
		}
		
	}
}
