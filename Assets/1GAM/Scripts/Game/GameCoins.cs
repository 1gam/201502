﻿using UnityEngine;
using System.Collections;

public class GameCoins : MonoBehaviour {

	public GameObject			coinPrefab;		//	
	public int					distance;			//	distance to objective
	public int					spread;
	public int					platforms;
	
	// Use this for initialization
	void Start () {
		
		for(int i = 0; i < Random.Range(50,platforms); i++) {
			
			float y = Random.Range(30,distance);
			float x = Random.Range(-spread,spread);
			
			for(int j = 0; j < 5; j++) {
				Vector2 pos = new Vector2(x,y-j);
				GameObject go = (GameObject)Instantiate(coinPrefab);
				go.transform.parent = transform;
				go.isStatic = true;
				go.transform.position = pos;
			}
		}	
	}
	
}
