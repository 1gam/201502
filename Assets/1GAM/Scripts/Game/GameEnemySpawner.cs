﻿using UnityEngine;
using System.Collections;

public class GameEnemySpawner : MonoBehaviour {

	public GameObject[]			spawnPrefabs;	//	
	public float				spawnDelay;		//	time between spawning
	public int					spawnRate;		//	number of enemies to spawn
	public Range				spawnRange;		//
	
	
	private SDN.Task			spawnTask;
	private GameObject			gamePlayer;
	
	void OnEnable() {
		gamePlayer = GameObject.Find("GamePlayer");
	}

	// Use this for initialization
	void Start () {
		spawnTask = new SDN.Task(spawnEnemies());
	}

	private IEnumerator spawnEnemies() {
		D.Log("[GameEnemySpawner] spawnEnemies");
		while(true) {
			yield return new WaitForSeconds(spawnDelay);
			D.Log("y: {0}", gamePlayer.transform.position.y);
			if (gamePlayer.transform.position.y > spawnRange.bottom) {
				if (gamePlayer.transform.position.y < spawnRange.top) {
					yield return StartCoroutine(spawnEnemy());
				}
			}
		}
	}	
	
	private IEnumerator spawnEnemy() {
		for(int i = 0; i < spawnRate; i++) {
			float y = gamePlayer.transform.position.y + 50 + Random.Range(-25,25);
			float x = 0;
			GameObject go = (GameObject)Instantiate(spawnPrefabs[Random.Range(0,spawnPrefabs.Length)]);
			go.transform.position = new Vector3(x,y,0);
		}
		yield return null;
	}
}
