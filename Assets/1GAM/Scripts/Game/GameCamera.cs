﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {

	public GameObject			Target;
	public float				Clamp;
	public float 				Speed;
	public Vector3				Offset;
	
	private tk2dCamera			gameCamera;
	private float				gameZoom;
	
	void OnEnable() {
		gameCamera = transform.GetComponent<tk2dCamera>();
		gameZoom = gameCamera.ZoomFactor;
	}

	void FixedUpdate() {
		Vector3 newpos = Target.transform.position;
		newpos += Offset;
		newpos += Offset;
		float x = Mathf.Clamp(newpos.x, -Clamp, Clamp);
		Vector3 pos = new Vector3(x, Mathf.Clamp(newpos.y,4,99999), newpos.z);
		pos = Vector3.Lerp(transform.position, pos, Speed * Time.deltaTime);
		
		transform.position = pos;
		
		float zoom = gameZoom - (Target.rigidbody2D.velocity.magnitude/5.0f);
		gameCamera.ZoomFactor = Mathf.Lerp(gameCamera.ZoomFactor, zoom, Time.deltaTime);
		
	}
}
