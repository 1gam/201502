﻿using UnityEngine;
using System.Collections;

public class GameCoin : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Player") {
			GameManager.Instance.GameCoins += 1;
			GameManager.Instance.GameScore += 10;
			GameManager.Instance.GameSounds.PlayCoin();
			Destroy(gameObject);
		}
	}
}
