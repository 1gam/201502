﻿using UnityEngine;
using System.Collections;

public class GameEnemy : MonoBehaviour {

	public float			enemySpeed;
	private SDN.Task		enemyTask;
	
	private float			speed;
	
	// Use this for initialization
	void Start () {
		enemyTask = new SDN.Task(move());	
		//Destroy(gameObject,15.0f);
		int r = Random.Range(0,2);
		
		if (r > 0) {
			speed = enemySpeed * -1;
			transform.position = transform.position + new Vector3(20,0,0);
		} else {
			speed = enemySpeed;
			transform.position = transform.position + new Vector3(-20,0,0);
		}
	}
	
	private IEnumerator move() {
		while(true) {
			yield return new WaitForSeconds(1);
			rigidbody2D.AddForce(transform.right * (speed * 25));
			if (transform.position.x > 50 || transform.position.x < -50) {
				Destroy(gameObject);
			}
		}
	}
	
	
	void OnDestroy() {
		enemyTask.Stop();
	}
	
}
