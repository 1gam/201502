﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public tk2dTextMesh		gameOver;
	public tk2dTextMesh		gameWinner;
	public tk2dUIItem		gameButton;
	
	private bool			Active;
	

	void OnEnable() {
		D.Log("[GameController] OnEnable");
		
		GameManager.Instance.GameController = this;
		
		gameOver.renderer.enabled = false;
		gameWinner.renderer.enabled = false;
		gameButton.gameObject.SetActive(false);
		
		GameManager.Instance.GameScore = 0;
		GameManager.Instance.GameCoins = 0;
		GameManager.Instance.GameFuel = 0;
		GameManager.Instance.GameDistance = 0;	//	distance to moon
		GameManager.Instance.GameScale = 0;	//	y scale in relation to distance of moon (1000 vector.y units * 380 = 380000)
	}

	void Start() {
		D.Log("[GameController] Start");
		StartGame();
		Active = true;
	}
	
	public void StartGame() {
		D.Log("[GameController] StartGame");
		
		GameManager.Instance.GameCoins = 0;
		GameManager.Instance.GameFuel = 220;
		GameManager.Instance.GameDistance = 78000000;
		GameManager.Instance.GameScale = 19500;
		GameManager.Instance.GameScore = 0;
	}
	
	void Update() {
	
		if (!Active) return;
	
		if (GameManager.Instance.GameFuel <= 0) {
			Active = false;
			GameLoser();
		}
		
		if (GameManager.Instance.PlayerController.gameObject.transform.position.y > 4000) {
			Active=false;
			GameWinner();
		}
	}
	
	public void StopGame() {
		GameManager.Instance.PlayerController.Paused = true;
		GameManager.Instance.PlayerController.rigidbody2D.isKinematic = true;
		gameButton.gameObject.SetActive(true);
	}
	
	public void GameWinner() {
		StopGame();
		gameWinner.renderer.enabled = true;
		GameManager.Instance.GameSounds.PlayWin();
	}
	
	public void GameLoser() {
		StopGame();
		gameOver.renderer.enabled = true;
		GameManager.Instance.GameSounds.PlayLose();
	}
	
	public void PlayAgain() {
		Application.LoadLevel(Application.loadedLevel);
	}
	
	
}
