﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlayerController : MonoBehaviour {

	public float			Force;
	public float			Speed;
	public float 			MaxVelocity;
	public bool				Paused;
	public GameObject		SmokePrefab;
	public int				smokeControl;
	
	private int				smoke;
	
	void OnEnable() {
		GameManager.Instance.PlayerController = this;
	}
	
	
	void FixedUpdate() {
		
		if (Paused) {
			audio.volume = 0.0f;
			return;
		}
		
		//gameObject.layer = 9; // i am in the player layer
		
		bool thrust = false;
		
		if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D)) {
			thrust = true;
		}
		
		if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow)) {
			thrust = true;
		}
		
		//	2 fingers on tablet = thrust
		if (Input.touchCount > 1) {
			thrust = true;
		}
		
		//	if we want to thrust otherwise normal player rotation using horizontal axis
		if (thrust) {
			rigidbody2D.AddForce(transform.up * Force);
			//gameObject.layer = 2; // i am in the ignore raycast layer
			audio.volume = 1.0f;
			GameManager.Instance.GameFuel -= 0.01f;
			smoke += 1;
			if (smoke > smokeControl) {
				Instantiate(SmokePrefab, transform.position, Quaternion.identity);
				smoke = 0;
			}
			
		} else {
		
			if (Input.touchCount > 0) {
				if (Input.GetTouch(0).position.x < Screen.width/2) {
					transform.Rotate(new Vector3(0,0,-1*-Speed));	
					audio.volume = 0.0f;
				}
				if (Input.GetTouch(0).position.x > Screen.width/2) {
					transform.Rotate(new Vector3(0,0,1*-Speed));	
					audio.volume = 0.0f;
				}
			}
			
			transform.Rotate(new Vector3(0,0,Input.GetAxis("Horizontal")*-Speed));	
			audio.volume = 0.0f;
		}
		
		//	not too fast!
		if (rigidbody2D.velocity.magnitude > MaxVelocity) {
			rigidbody2D.velocity = rigidbody2D.velocity.normalized * MaxVelocity;
		}
	}
	
#if UNITY_EDITOR	
	void OnDrawGizmosSelected() {
		string s = "pos: " + transform.position + "\n";
		s += "vel: " + rigidbody2D.velocity.magnitude.ToString() + "\n";
		Handles.Label(transform.position + Vector3.down * 2, s);
	}
#endif

}
